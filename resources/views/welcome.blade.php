<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Epic ToDo App - Powerfully simple ToDo App | No Login/Register required</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="{{ asset('/css/app.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="base-color lighten-1" role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="#" class="brand-logo">Epic ToDo App</a>

        <ul class="right hide-on-med-and-down">
            <li><a href="{{ route('create_todo') }}">Create a List</a></li>
        </ul>
    </div>
  </nav>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center secondary-color-text">
          A powerfully simple ToDo app
      </h1>
      <div class="row center">
        <h5 class="header col s12 light">No sign-up required!</h5>
      </div>
      <div class="row center">
        <a href="{{ route('create_todo') }}" id="download-button" class="btn-large waves-effect waves-light secondary-color">Get Started</a>
      </div>
      <br><br>

    </div>
  </div>


  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center secondary-color-text"><i class="material-icons">flash_on</i></h2>
            <h5 class="center">With you everywhere</h5>

            <p class="light">
                You can access your lists on your phone, tablet, or computer whenever you want. Just copy the link and paste it on your other device.
            </p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center secondary-color-text"><i class="material-icons">group</i></h2>
            <h5 class="center">No login required</h5>

            <p class="light">You don't need to login/register, your work will be saved locally on your device or at a special URL if you wish.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center secondary-color-text"><i class="material-icons">smartphone</i></h2>
            <h5 class="center">Even better on mobile</h5>

            <p class="light">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua.</p>
          </div>
        </div>
      </div>

    </div>
    <br><br>
  </div>

  <footer class="page-footer secondary-color">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">About</h5>
          <p class="grey-text text-lighten-4">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.
          </p>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="purple-text text-lighten-3" href="https://shryder.me/">Shryder</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/vanilla/init.js') }}"></script>

  </body>
</html>
