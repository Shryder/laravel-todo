<?php

namespace App\Http\Controllers;

use App\Todo;
use App\TodoList;
use Illuminate\Http\Request;

class TodoListController extends Controller {
	public function store(Request $request, $todo_id) {
		$request->validate([
			'name' => 'required|max:200',
		]);

		$todo = Todo::findOrFail($todo_id);

		$todo_list = new TodoList();
		$todo_list->name = $request->name;
		$todo_list->todo_id = $todo->id;

		$todo_list->save();

		return response()->json([
			'success' => true,
			'data' => $todo_list,
		]);
	}
}
