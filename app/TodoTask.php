<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class TodoTask extends Model {
	protected $fillable = ['title', 'completed'];
	protected $casts = ['completed' => 'boolean'];

	protected static function boot(): void{
		parent::boot();

		static::addGlobalScope('order', function (Builder $builder) {
			$builder->orderBy('created_at', 'asc');
		});
	}
}
