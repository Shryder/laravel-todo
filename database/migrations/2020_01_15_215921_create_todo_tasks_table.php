<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodoTasksTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('todo_tasks', function (Blueprint $table) {
			$table->uuid('id')->primary();

			$table->string('title')->nullable();
			$table->boolean('completed')->default(false);
			$table->uuid('todo_list_id');

			$table->foreign('todo_list_id')->references('id')->on('todo_lists')->onDelete('CASCADE');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('todo_tasks');
	}
}
