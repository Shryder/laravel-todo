<?php

namespace App\Http\Controllers;

use App\Todo;
use App\TodoList;
use Illuminate\Http\Request;

class TodoController extends Controller {
	public function todo($id) {
		$todo = Todo::where('id', $id)->with('lists.tasks')->firstOrFail();

		return response()->json([
			'success' => true,
			'data' => $todo,
		]);
	}

	public function show($id) {
		return view('todos.show', compact('id'));
	}

	public function create() {
		return view('todos.new');
	}

	public function store() {
		$todo = Todo::create();

		$todo_list = new TodoList();
		$todo_list->name = 'Untitled';
		$todo_list->todo_id = $todo->id;
		$todo_list->save();

		return redirect()->route('todos.show', ['id' => $todo->id]);
	}
}
