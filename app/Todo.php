<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Todo extends Model {
	protected $fillable = ['name'];

	public function lists() {
		return $this->hasMany('App\TodoList');
	}
}
