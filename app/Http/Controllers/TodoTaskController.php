<?php

namespace App\Http\Controllers;

use App\TodoList;
use App\TodoTask;
use Illuminate\Http\Request;

class TodoTaskController extends Controller {
	public function update(Request $request, $task_id) {
		$task = TodoTask::findOrFail($task_id);

		$task->update($request->all());

		return response()->json([
			'success' => true,
			'data' => $task,
		]);
	}

	public function destroy($task_id) {
		TodoTask::destroy($task_id);

		return response()->json([
			'success' => true,
		]);
	}

	public function store(Request $request, $todo, $list) {
		$request->validate([
			'title' => 'present',
		]);

		$list = TodoList::findOrFail($list);

		$task = new TodoTask();
		$task->todo_list_id = $list->id;
		$task->title = $request->title;
		$task->completed = false;

		$task->save();

		return response()->json([
			'success' => true,
			'data' => $task,
		]);
	}
}
