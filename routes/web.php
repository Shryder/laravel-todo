<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('welcome');
});

Route::get('/get_started', 'TodoController@store')->name('create_todo');
Route::view('/todo/local', 'todo');
Route::get('/todo/{id}', 'TodoController@show')->name('todos.show');

Route::get('/api/todo/{id}', 'TodoController@todo');
Route::post('/todo/{id}/lists', 'TodoListController@store');
Route::patch('/api/tasks/{task}', 'TodoTaskController@update');
Route::delete('/api/tasks/{task}', 'TodoTaskController@destroy');
Route::post('/todo/{todo_id}/lists/{list_id}/tasks', 'TodoTaskController@store');
