<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class TodoList extends Model {
	protected $fillable = ['name'];
	protected $with = ['tasks']; // Always include the tasks with the list.

	public function tasks() {
		return $this->hasMany('App\TodoTask');
	}

	protected static function boot(): void{
		parent::boot();

		static::addGlobalScope('order', function (Builder $builder) {
			$builder->orderBy('created_at', 'asc');
		});
	}
}
