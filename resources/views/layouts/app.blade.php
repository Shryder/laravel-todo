<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
        <title>Epic ToDo App - Powerfully simple ToDo App | No Login/Register required</title>
        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{ asset('/css/app.css') }}" type="text/css" rel="stylesheet" media="screen,projection" />
    </head>
    <body>
        <div id='app'>
            @yield('content')
        </div>

        <!--  Scripts-->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/vanilla/init.js') }}"></script>
    </body>
</html>
