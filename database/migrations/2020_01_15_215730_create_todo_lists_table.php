<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodoListsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('todo_lists', function (Blueprint $table) {
			$table->uuid('id')->primary();

			$table->string('name')->default("");
			$table->uuid('todo_id');

			$table->foreign('todo_id')->references('id')->on('todos')->onDelete('CASCADE');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('todo_lists');
	}
}
